<?php
// ******************************************************************
// This is the standard TypoScript bookingpar table, tt_content
// ******************************************************************
if (!defined ('TYPO3')) 	die ('Access denied.');

/***************
 * Plugin
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Planningcenter',
    'Planningcenter',
    'Get Liturgy'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['planningcenter_planningcenter'] = 'recursive';


