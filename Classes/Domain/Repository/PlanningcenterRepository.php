<?php

namespace Parousia\Planningcenter\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Parousia\Planningcenter\Domain\Model\Liturgy;
use Parousia\Planningcenter\Domain\Model\Orderitem;
use DateTime;

//class PlanningcenterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
class PlanningcenterRepository 
{
    /**
     * return array with liturgy
    */
    public function findOrder()
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].$offset."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/planningcenter/Classes/Controller/debug.log');
		$apikey= GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('planningcenter','apikey');

		$curl = curl_init();
		// Read plan:
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.planningcenteronline.com/services/v2/service_types/1039583/plans?filter=future&per_page=1',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		   'Authorization: '. $apikey
		  ),
		));
              
		$response = curl_exec($curl);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].'findorder response: '.$response."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//print_r($response );
		$plan=json_decode($response);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].'findorder plan: '.(string)$plan."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		
		$data = $plan->data[0];
		$planid=$data->id;
		$attributes=$data->attributes;
		$seriestitle=$attributes->series_title;
		$title=$attributes->title;
		$date=new DateTime($attributes->dates);
		$Liturgy= new Liturgy();
		$Liturgy->setSeriesTitle($seriestitle);
		$Liturgy->setSermonTitle($title);
		$Liturgy->setDateService($date);
		
		// Read order:
		curl_setopt($curl,  CURLOPT_URL, 'https://api.planningcenteronline.com/services/v2/service_types/1039583/plans/'.$planid.'/items');
		
		$response = curl_exec($curl);
		
		$aResponse=json_decode($response);
		$datas=$aResponse->data;
		//print_r($datas);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'litury findOrder : '.print_r($datas,true)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/planningcenter/Classes/Controller/debug.log');		
		
		foreach ($datas as $data)
		{
			//echo "key:".$key;
			if ($data->attributes->service_position=="during" and $data->attributes->item_type!=="header")
			{
				$title=$data->attributes->title;
				$type=$data->attributes->item_type;
				if ($type=="item" and $title=="Einde stream") break; //end of order
				$orderitem= new Orderitem();
				$orderitem->setOrderTitle($title);
				$orderitem->setDescription($data->attributes->description);
				$orderitem->setType($data->attributes->item_type);
				$Liturgy->addOrderitems($orderitem);
			}
		}
		
		// Read teams
		curl_setopt($curl,  CURLOPT_URL, 'https://api.planningcenteronline.com/services/v2/service_types/1039583/plans/'.$planid.'/team_members');
		
		$response = curl_exec($curl);
		
		$aResponse=json_decode($response);
		$datas=$aResponse->data;
		//print_r($datas);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'litury findOrder : '.print_r($datas,true)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/planningcenter/Classes/Controller/debug.log');		
		
		foreach ($datas as $data)
		{
			//echo "key:".$key;
			//if ($data->attributes->status=="C")
			//{
				if ($data->attributes->team_position_name=="Oudste van dienst")
					$Liturgy->setOudste($data->attributes->name);
				if ($data->attributes->team_position_name=="Aanbiddingsleider")
					$Liturgy->setAanbiddingsleider($data->attributes->name);
				if ($data->attributes->team_position_name=="Regisseur")
					$Liturgy->setRegisseur($data->attributes->name);
				if ($data->attributes->team_position_name=="Spreker")
					$Liturgy->setSpreker($data->attributes->name);
				if ($data->attributes->team_position_name=="Kindermoment")
					$Liturgy->setKindermoment($data->attributes->name);
			//}
		}
		
		curl_close($curl);
		 
		return $Liturgy;
	}
		 
}

