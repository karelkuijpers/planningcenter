<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Planningcenter',
         'Planningcenter',
         [
             \Parousia\Planningcenter\Controller\PlanningcenterController::Class => 'getLiturgy',
         ],
         // non-cacheable actions
		 [
             \Parousia\Planningcenter\Controller\PlanningcenterController::Class => 'getLiturgy',
         ]
     );

