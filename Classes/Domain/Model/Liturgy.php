<?php

namespace Parousia\Planningcenter\Domain\Model;

use DateTime;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Georg Ringer, montagmorgen.at
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News
 */
class Liturgy extends AbstractEntity
{

    /**
     * seriesTitle
     *
     * @var string
     */
    protected $seriesTitle = '';

    /**
     * sermonTitle
     *
     * @var string
     */
    protected $sermonTitle = '';

    /**
     * oudste
     *
     * @var string
     */
    protected $oudste = '';

    /**
     * aanbiddingsleider
     *
     * @var string
     */
    protected $aanbiddingsleider = '';

    /**
     * regisseur
     *
     * @var string
     */
    protected $regisseur = '';

    /**
     * spreker
     *
     * @var string
     */
    protected $spreker = '';

    /**
     * kindermoment
     *
     * @var string
     */
    protected $kindermoment = '';

    /**
     * dateService
     *
     * @var \DateTime
     */
    protected $dateService = null;

    /**
     * the order item of the service
     *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Parousia\Planningcenters\Domain\Model\Orderitem>
	*/
    protected $orderitems;

	
    /**
     * Initialize liturgy
     *
     * @return \Parousia\Planningcenter\Domain\Model\Liturgy
     */
    public function __construct()
    {
        $this->orderitems = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }


    /**
     * Sets the seriesTitle
     *
     * @param string $seriesTitle
     * @return void
     */
    public function setSeriesTitle($seriesTitle)
    {
        $this->seriesTitle = $seriesTitle;
    }
    /**
     * Returns the seriesTitle
     *
     * @return string $seriesTitle
     */
    public function getSeriesTitle()
    {
        return $this->seriesTitle;
    }

    /**
     * Sets the sermonTitle
     *
     * @param string $sermonTitle
     * @return void
     */
    public function setSermonTitle($sermonTitle)
    {
        $this->sermonTitle = $sermonTitle;
    }

    /**
     * Returns the sermonTitle
     *
     * @return string $sermonTitle
     */
    public function getSermonTitle()
    {
        return $this->sermonTitle;
    }
	
    /**
     * Sets the oudste
     *
     * @param string $oudste
     * @return void
     */
    public function setOudste($oudste)
    {
        $this->oudste = $oudste;
    }

    /**
     * Returns the oudste
     *
     * @return string $oudste
     */
    public function getOudste()
    {
        return $this->oudste;
    }
	
    /**
     * Sets the aanbiddingsleider
     *
     * @param string $aanbiddingsleider
     * @return void
     */
    public function setAanbiddingsleider($aanbiddingsleider)
    {
        $this->aanbiddingsleider = $aanbiddingsleider;
    }

    /**
     * Returns the aanbiddingsleider
     *
     * @return string $aanbiddingsleider
     */
    public function getAanbiddingsleider()
    {
        return $this->aanbiddingsleider;
    }
	
    /**
     * Sets the regisseur
     *
     * @param string $regisseur
     * @return void
     */
    public function setRegisseur($regisseur)
    {
        $this->regisseur = $regisseur;
    }

    /**
     * Returns the regisseur
     *
     * @return string $regisseur
     */
    public function getRegisseur()
    {
        return $this->regisseur;
    }
	
    /**
     * Sets the spreker
     *
     * @param string $spreker
     * @return void
     */
    public function setSpreker($spreker)
    {
        $this->spreker = $spreker;
    }

    /**
     * Returns the spreker
     *
     * @return string $spreker
     */
    public function getSpreker()
    {
        return $this->spreker;
    }

	
    /**
     * Sets the kindermoment
     *
     * @param string $kindermoment
     * @return void
     */
    public function setKindermoment($kindermoment)
    {
        $this->kindermoment = $kindermoment;
    }

    /**
     * Returns the kindermoment
     *
     * @return string $kindermoment
     */
    public function getKindermoment()
    {
        return $this->kindermoment;
    }
    /**
     * Get dateService
     *
     * @return DateTime
     */
    public function getDateService(): DateTime
    {
        return $this->dateService;
    }

    /**
     * set DateService
     *
     * @param DateTime $dateservice datetime
     *
     * @return void
     */
    public function setDateService($dateService): void
    {
        $this->dateService = $dateService;
    }

	/**
     * Returns the orderitems
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getOrderitems()
    {
        return $this->orderitems;
    }


    /**
     * Set Orderitems 
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $orderitems
     */
    public function setOrderitems(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $orderitems)
    {
        $this->orderitems = $orderitems;
    }

    /**
     * Add a orderitem 
     *
     * @param Orderitem $orderitem
     */
    public function addOrderitems(Orderitem $orderitem)
    {
        if ($this->getOrderitems() === null) {
            $this->orderitems = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->orderitems->attach($orderitem);
    }



}
