<?php

namespace Parousia\Planningcenter\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Georg Ringer, montagmorgen.at
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News
 */
class Orderitem extends AbstractEntity
{

    /**
     * orderTitle
     *
     * @var string
     */
    protected $orderTitle = false;

    /**
     * description
     *
     * @var string
     */
    protected $description = false;

    /**
     * type
     *
     * @var string
     */
    protected $type = null;


    /**
     * Sets the orderTitle
     *
     * @param string $orderTitle
     * @return void
     */
    public function setOrderTitle($orderTitle)
    {
        $this->orderTitle = $orderTitle;
    }
    /**
     * Returns the orderTitle
     *
     * @return string $orderTitle
     */
    public function getOrderTitle()
    {
        return $this->orderTitle;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

}
