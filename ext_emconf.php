<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Planningcenter',
    'description' => 'Showing data from planning center extension, based on extbase & fluid.',
    'category' => 'fe',
    'author' => 'Karel Kuijpers',
    'author_email' => 'karelkuijpers@gmail.com',
    'state' => 'stable',
    'version' => '11.0.2',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.13-11.5.99',
        ],
        'conflicts' => [],
    ],
];
