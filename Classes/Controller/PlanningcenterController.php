<?php

namespace Parousia\Planningcenter\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Parousia\Planningcenter\Domain\Repository\PlanningcenterRepository;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Parousia\planningcenter\Controller\NewsController
 */
//class EventsController extends \GeorgRinger\News\Controller\NewsController
class PlanningcenterController extends ActionController
{
    /**
     * @var 
     */
	protected $planningcenterRepository;

    public function __construct() 
	{
        $this->planningcenterRepository = GeneralUtility::makeInstance(PlanningcenterRepository::class);		
    }

    /**
     * Show Liturgy view
     *
     */
    public function getLiturgyAction(): ResponseInterface
    {
        $liturgyRecords = $this->planningcenterRepository->findOrder();

	    $assignedValues = [
            'liturgy' => $liturgyRecords,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

    }

}
